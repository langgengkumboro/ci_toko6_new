
<body>

  <div style="color: red"><?=validation_errors(); ?></div>
  <form action="<?=base_url()?>Karyawan/inputkaryawan" method="POST" enctype="multipart/form-data">
<table width="46%" border="0" cellspacing="0" cellpadding="5" bgcolor="green">
  <tr>
    <td width="43%">Nik</td>
    <td width="5%">:</td>
    <td width="52%">
      <input type="text" name="nik" id="nik" value="<?= set_value('nik');?>" />
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_karyawan" id="nama_karyawan"value="<?= set_value('nama_karyawan');?>" />
     </td>
</td>
  </tr>
 
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input type="text" name="tempat_lahir" id="tempat_lahir" value="<?= set_value('tempat_lahir');?>" />
     </td>
</td>
  </tr>
  
  
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin" value="<?= set_value('jenis_kelamin');?>" />>
      <option value="L">Laki - Laki</option>
      <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  
  
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tgl" id="tgl" value="<?= set_value('tgl');?>" />>
      <?php 
			for($tgl=1;$tgl<=31;$tgl++){
					
	  ?>
	  <option value="<?=$tgl;?>"><?=$tgl;?></option>
	  <?php 
			 }
	  ?>
      </select>
      
      
      <select name="bulan" id="bulan">
      <?php
		$n_bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',							                           'Oktober','September','November','Desember');
							
				for($bulan=0;$bulan<12;$bulan++){	
					if($bulan+1 == date('m')){
							$slc_bulan = 'SELECTED';
					}else{
							$slc_bulan = '';
		  }
				
	 ?>
	<option <?=$slc_bulan;?> value="<?=$bulan+1;?>"><?=$n_bulan[$bulan];?> </option>
		
	<?php
			}
	 ?>	
    </select>
      
     
     <select name="tahun" id="tahun">
      <?php
				for($tahun=date('Y')-60;$tahun<=date('Y')-15;$tahun++){	
					
		?>
				<option value="<?=$tahun;?>"><?=$tahun;?> </option>
	 <?php
				}
			?>  
      
      </select>
      
    </td>
  </tr>
  
    <tr>
    <td>Telpon</td>
    <td>:</td>
    <td>
      <input type="text" name="telpon" id="telpon" value="<?= set_value('telpon');?>" />
    </td>
  </tr>

 <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= set_value('alamat');?>" ></textarea>
    </td>
  </tr>
  
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      
        <select id="kode_jabatan" name="kode_jabatan" value="<?= set_value('kode_jabatan');?>">
          <?php foreach ($data_jabatan as $data) { ?>
            <option value="<?=$data->kode_jabatan; ?>">
            <?= $data->nama_jabatan; ?></option>

            <?php } ?>
          
        </select>
     
    </td>
  </tr>

  <tr>
    <td>Upload Foto</td>
    <td>:</td>
    <td style="text-align: left;">
      <input type="file" name="image" id="image">
      
    </td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="Simpan" />
      <input type="reset" name="reset" id="reset" value="Reset" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="kembali ke Menu Sebelumnya" />
      <a href="<?=base_url();?>Karyawan/listkaryawan"><font color="white">kembali ke Menu Sebelumnya</font></a>
  </tr>
  
</table>
</form>

</body>
