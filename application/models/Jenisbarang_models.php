<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenisbarang_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "jenis_barang";
	public function rules(){
		return
		[
					[ 
						'field'=> 'kode_jenis',
						'label' => 'Kode Jenis',
						'rules' => 'required|max_length[5]',
						'errors' => [
							'requered' => 'kode Jenis tidak boleh kosong.',
							'max_length' => 'kode Jenis tidak boleh lebih dari 5 karakter.',
						]
					],
					[
						'field' => 'nama_jenis',
						'label'  => 'Nama Jenis',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama Jenis tidak Boleh kosong.',
						]
					]
							];
	}

	public function tampilDataJenisbarang()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataJenisbarang2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM jenis_barang WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataJenisbarang3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_jenis', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			
			$data['kode_jenis'] =$this->input->post('kode_jenis');
			$data['nama_jenis'] =$this->input->post('nama_jenis');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
		}
	public function detailjenisbarang($kode_jenis)
		{
			$this->db->select('*');
			$this->db->where('kode_jenis', $kode_jenis);
			$this->db->where('flag', 1);
			$result = $this->db->get($this->_table);
			return $result->result();
		}

		public function update($kode_jenis)
		{
			
			$data['nama_jenis'] =$this->input->post('nama_jenis');
			$data['flag'] =1;

			$this->db->where('kode_jenis', $kode_jenis);
			$this->db->update($this->_table, $data);
		}

	public function delete($kode_jenis)
	
		{
			$this->db->where('kode_jenis',$kode_jenis);
			$this->db->delete($this->_table);
		}
	public function tampilDataJenisbarangPagination($perpage, $uri, $data_pencarian)
	{
		// echo "<pre>";
		// print_r($data_pencarian); die();
		// echo "</pre>";
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_jenis', $data_pencarian);
		}
		$this->db->order_by('kode_jenis','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return$get_data->result();
		}else{
			return null;
		}

	}
public function tombolpagination($data_pencarian)
	{
		//echo "<pre>";
		//print_r($data_pencarian); die();
		//echo "</pre>";
		//cari jmlh data berdasarkan data pencarian
		$this->db->like('nama_jenis', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		//echo "<pre>";
		//print_r($hasil); die();
		//echo "</pre>";

		//pagination limit
		$pagination['base_url']		= base_url().'Jenisbarang/data_jenisbarang/load/';
		$pagination['total_rows']	=$hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		//custom pagging configuration
		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page |';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page |';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page |';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page |';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<span class="curlink">';
		$pagination['cur_tag_close']	= '</span>';

		$pagination['num_tag_open']		= '<span class="numlink">';
		$pagination['num_tag_close']	= '</span>';

		$this->pagination->initialize($pagination);

		$hasil_pagination	= $this->tampilDataJenisbarangPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}

} 