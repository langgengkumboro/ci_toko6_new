<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait (manggil pertama kali scrip di jalanin)
		$this->load->model("Karyawan_model");
		$this->load->model("Jabatan_models");


		//load validasi
		$this->load->library('form_validation');

		//cek sesi login
		//$user_login	= $this->session->userdata();
		//if()
		//load validasi
	
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->listKaryawan();


	}
	public function listKaryawan()
	{
		//$data['data_Karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		//$data['content']	='forms/list_karyawan';
		//$this->load->view('Home_2', $data);


		if (isset($_POST['cari_data'])) {
			$data['kata_pencarian'] = $this->input->post('cari_nama');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] =$this->session->userdata('session_pencarian');
		}
		// 	echo "<pre>";
		// print_r($this->session->userdata()); die();
		// echo "</pre>";

		$data['data_Karyawan']	= $this->Karyawan_model->tombolpagination($data
			['kata_pencarian']);

		$data['content']	= 'forms/list_karyawan';
		$this->load->view('Home_2', $data);
	}

	public function detailKaryawan($nik)
	{
		$data['data_Karyawan'] =$this->Karyawan_model->detail($nik);
		$data['content']       ='forms/detailkaryawan';
		$this->load->view('home_2', $data);
	}

	public function inputkaryawan()
	{
		$data['data_jabatan'] = $this->Jabatan_models->tampilDataJabatan();

		//if (!empty($_REQUEST)){
		//	$m_karyawan = $this->Karyawan_model;
		//	$m_karyawan->save();
		//	redirect("Karyawan/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Karyawan_model->rules());

		if ($validation->run()){
			$this->Karyawan_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Karyawan/index", "refresh");
			}
			$data['content']       ='forms/inputkaryawan';
			$this->load->view('home_2', $data);
		
	}
	public function editkaryawan($nik)
	{	
		$data['data_jabatan'] 		= $this->Jabatan_models->tampilDataJabatan();
		$data['detail_karyawan']	= $this->karyawan_model->detail($nik);
		
		//if (!empty($_REQUEST)) {
		//		$m_karyawan = $this->karyawan_model;
		//		$m_karyawan->update($nik);
		//		redirect("karyawan/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Karyawan_model->rules());

		if ($validation->run()){
			$this->Karyawan_model->update($nik);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Karyawan/index", "refresh");
			}	
			
		
			$data['content']       ='forms/editkaryawan';
			$this->load->view('home_2', $data);	
	}

	public function delete($nik)
	{
		$m_karyawan = $this->Karyawan_model;
		$m_karyawan->delete($nik);	
		redirect("Karyawan/index", "refresh");	
	}
}
